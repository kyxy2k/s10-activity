package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}


	//@RequestMapping(value="/posts", method = RequestMethod.GET)
    @GetMapping("/posts")
	public String getPosts(){
		return "All posts retrieved";
	}

	//@RequestMapping(value="/posts", method = RequestMethod.POST)
    @PostMapping("/posts")
	public String createPost(){
		return "New post created";
	}

	//@RequestMapping(value = "/posts/{postid}", method = RequestMethod.GET)
    @GetMapping("/posts/{postid}")
	public String getPost(@PathVariable Long postid){
		return "Viewing details of post" + postid;
	}

	//@RequestMapping(value = "/posts/{postid}", method = RequestMethod.DELETE)
    @DeleteMapping("/posts/{postid}")
	public String deletePost(@PathVariable Long postid){
		return "The post" + postid + "has been deleted";

	}

    //@RequestMapping(value = "/posts/{postid}", method = RequestMethod.PUT)
    @PutMapping("/posts/{postid}")
    @ResponseBody
    public Post updatePost(@PathVariable Long postid, @RequestBody Post post){
        return post;
    }

   // @RequestMapping(value = "/myPosts", method = RequestMethod.GET)
    @GetMapping("/myPosts")
	public String getMyposts(@RequestHeader(value = "Authorization") String user){
		if(user.isEmpty()){
			user = "Unauthorized Access";
		}
		else {
			return "Posts for " + user + " have been retrieved.";
		}
		return user;
	}
//ACTIVITY S10

	//a. GET all users
	@GetMapping("/users")
	public String getAllUsers(){
		return "All users retrieved";
	}

	//b. Create User
	@PostMapping("/users")
	public String postUser(){
		return "New user created";
	}
	//c. Get a specific User
	@GetMapping("/users/{user}")
	public String getUser(@PathVariable String user){
		return "name: "+user;
	}
	//d. Delete user
	@DeleteMapping("/users/{userid}")
	public String deleteUser(@PathVariable String userid,@RequestHeader(value="authorization") String user){
		if(user.isEmpty()){
			user = "Unauthorized access";
		}
		else {
			user = "The user " + userid + " has been deleted";
		}
		return user;
	}

	//e. Update user
	@PutMapping("/users/{userid}")
	@ResponseBody
	public User putUser(@PathVariable String userid,@RequestBody User user){
		return user;
	}




}
